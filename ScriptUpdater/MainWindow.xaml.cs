﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

namespace ScriptUpdater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var applicationDirectory = AppDomain.CurrentDomain.BaseDirectory;
            if (!File.Exists($"{applicationDirectory}\\Configuration.xml"))
            {
                CreateConfigurationFile();
            }
            else
            {
                try
                {
                    var scriptsDocument = XDocument.Load($"Configuration.xml");
                    PathTo.Text = scriptsDocument.Root.Element("PathToUEMScriptsFolder").Value;
                }
                catch
                {
                    File.Delete($"Configuration.xml");
                    CreateConfigurationFile();
                    MessageBox.Show($"Configuration file is broken." +
                        $"New empty configuration file created.");
                }
            }
        }

        private void SelectPathFrom_Click(object sender, RoutedEventArgs e)
        {
            SelectFolder(PathFrom);
        }

        private void SelectPathTo_Click(object sender, RoutedEventArgs e)
        {
            SelectFolder(PathTo);
        }

        private void UpdateScripts_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var pathFrom = PathFrom.Text;
                if (string.IsNullOrEmpty(pathFrom))
                {
                    MessageBox.Show($"Please specify the path to generated UEM scripts.");
                    return;
                }

                var pathTo = PathTo.Text;
                if (string.IsNullOrEmpty(pathFrom))
                {
                    MessageBox.Show($"Please specify the path to exising UEM scripts.");
                    return;
                }

                var scriptNumberText = ScriptNumber.Text;
                if (string.IsNullOrEmpty(scriptNumberText))
                {
                    MessageBox.Show($"Please specify the first script number for the new scripts.");
                    return;
                }               

                var comment = Comment.Text;
                if (string.IsNullOrEmpty(comment))
                {
                    MessageBox.Show($"Please input a comment which describes changes included in new scripts.");
                    return;
                }

                if (!int.TryParse(scriptNumberText, out var scriptNumberToWrite))
                {
                    MessageBox.Show($"Script number should be an integer.");
                    return;
                }

                if (!File.Exists($"{pathTo}\\UEMConsole.xml"))
                {
                    MessageBox.Show($"Incorrect folder with UEM scripts specified.{Environment.NewLine}" +
                        "The folder should be the one containing UEMConsole.xml file.");
                    return;
                }

                var pathsToGeneratedScripts = Directory.GetFiles(PathFrom.Text).ToList();

                if (pathsToGeneratedScripts.Count < 2)
                {
                    MessageBox.Show($"Cannot update the scripts. {Environment.NewLine} " +
                        $"The folder with generated scripts should contain at least two files.");
                    return;
                }

                var pathsToScriptsFile = pathsToGeneratedScripts.Where(path => path.EndsWith("Scripts.xml"));

                if (pathsToScriptsFile.Count() > 1)
                {
                    MessageBox.Show($"Cannot update the scripts. {Environment.NewLine} " +
                        $"There are more than one \"Scripts.xml\" file in folder with new scripts.");
                    return;
                }

                if (!pathsToScriptsFile.Any())
                {
                    MessageBox.Show($"Cannot update the scripts. {Environment.NewLine} " +
                        "There is no \"Scripts.xml\" file in folder with new scripts.");
                    return;
                }

                var pathToScriptsFile = pathsToScriptsFile.Single();

                var xDocument = XDocument.Load(pathToScriptsFile);
                var scriptElements = xDocument.Root.Elements();

                if (!scriptElements.Any())
                {
                    MessageBox.Show($"Cannot update the scripts.{Environment.NewLine} " +
                        $"Invalid \"Scripts.xml\" file.{Environment.NewLine}." +
                        $"No elements found in the root element.");
                    return;
                }

                foreach (var element in scriptElements)
                {
                    if (element.Name != "Script")
                    {
                        MessageBox.Show($"Cannot update the scripts.{Environment.NewLine} " +
                        $"Invalid \"Scripts.xml\" file.{Environment.NewLine}." +
                        $"All elements inside of the root element are supposed to be <Script> elements.");
                        return;
                    }
                }

                scriptElements.OrderBy(element => element.Attribute("Build").Value);

                var lastGeneratedScriptNumber = 0;

                List<XElement> newSchemaScripts = new List<XElement>();
                Dictionary<string, string> filesToMove = new Dictionary<string, string>();
                List<int> newScriptNumbers = new List<int>();

                foreach (var scriptElement in scriptElements)
                {
                    int.TryParse(scriptElement.Attribute("Build").Value, out var currentGeneratedScriptNumber);

                    if (lastGeneratedScriptNumber != 0 && lastGeneratedScriptNumber != currentGeneratedScriptNumber)
                    {
                        scriptNumberToWrite++;
                    }

                    var filePath = scriptElement.Attribute("FilePath").Value;
                    var pathsToScript = pathsToGeneratedScripts.Where(path => path.EndsWith(filePath));

                    if (!pathsToScript.Any() || pathsToScript.Count() > 1)
                    {
                        MessageBox.Show($"Cannot update the scripts.{Environment.NewLine} " +
                        $"Source folder does not contain file {filePath}, listed in Scripts.xml file.");
                        return;
                    }

                    var initialFileName = scriptElement.Attribute("Name").Value;
                    var newFileName = $"UEM_{scriptElement.Attribute("Name").Value}";
                    var pathToCurrentScript = pathsToScript.Single();
                    var updatedPathToFile = filePath
                        .Replace(currentGeneratedScriptNumber.ToString(), scriptNumberToWrite.ToString("D4"))
                        .Replace(initialFileName, newFileName);

                    newSchemaScripts.Add(new XElement("Script",
                        new XAttribute("Major", scriptElement.Attribute("Major").Value),
                        new XAttribute("Minor", scriptElement.Attribute("Minor").Value),
                        new XAttribute("Build", scriptNumberToWrite.ToString("D4")),
                        new XAttribute("Type", scriptElement.Attribute("Type").Value),
                        new XAttribute("Name", newFileName),
                        new XAttribute("FilePath", $"UEMConsole\\{updatedPathToFile}")));

                    filesToMove.Add(filePath, $"UEMConsole\\{updatedPathToFile}");

                    lastGeneratedScriptNumber = currentGeneratedScriptNumber;

                    if (!newScriptNumbers.Contains(scriptNumberToWrite))
                    {
                        newScriptNumbers.Add(scriptNumberToWrite);
                    }
                }

                if (!newSchemaScripts.Select(newScriptEntry => newScriptEntry.Attribute("FilePath").Value)
                    .SequenceEqual(filesToMove.Select(fileToMove => fileToMove.Value)))
                {
                    MessageBox.Show($"Cannot update the scripts.{Environment.NewLine} " +
                        $"Entries in Scripts.xml file do not match actual files in folder {pathFrom}");
                    return;
                }

                foreach (var fileToMove in filesToMove)
                {
                    File.Copy($"{PathFrom.Text}\\{fileToMove.Key}", $"{PathTo.Text}\\{fileToMove.Value}");
                }

                var scriptsDocument = XDocument.Load($"{PathTo.Text}\\UEMConsole.xml", LoadOptions.PreserveWhitespace);

                scriptsDocument.Root.Add(Environment.NewLine);
                scriptsDocument.Root.Add("  ");
                scriptsDocument.Root.Add(new XComment(Comment.Text));
                scriptsDocument.Root.Add(Environment.NewLine);
                foreach (var newScript in newSchemaScripts)
                {
                    scriptsDocument.Root.Add("  ");
                    scriptsDocument.Root.Add(newScript);
                    scriptsDocument.Root.Add(Environment.NewLine);
                }

                scriptsDocument.Save($"{PathTo.Text}\\UEMConsole.xml");

                MessageBox.Show($"Scripts are updated.{Environment.NewLine}" +
                    $"Script numbers used: {string.Join(", ", newScriptNumbers)}");
                return;
            }

            catch (Exception ex)
            {
                MessageBox.Show($"Unexpected exception: {ex.Message}");
                return;
            }            
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            var configuration = XDocument.Load($"Configuration.xml");
            configuration.Root.Element("PathToUEMScriptsFolder").Value = PathTo.Text;
            configuration.Save("Configuration.xml");
        }

        private void SelectFolder(TextBox textBox)
        {
            var dialog = new CommonOpenFileDialog
            {
                IsFolderPicker = true
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                textBox.Text = dialog.FileName;
            }
        }

        private void CreateConfigurationFile()
        {
            new XDocument(new XElement("Configuration", new XElement("PathToUEMScriptsFolder", ""))).Save("Configuration.xml");
        }
    }
}
